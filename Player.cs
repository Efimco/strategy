using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float Rate;
    public float RotationSpeed;
    public float MovementSpeed;

    [HideInInspector]
    public bool IsEmmit;
    [HideInInspector]
    public bool IsRotating;
    [HideInInspector]
    public bool IsMoving;

    private IStrategy Move = new MoveStrategy();
    private IStrategy Rotate = new RotateStrategy();
    private IStrategy Emmit = new EmmitStrategy();



    private void Update()
    {
        if (IsEmmit)
        {
            Emmit.Perform(transform, Rate);
            
        }
        if (IsRotating)
        {
            Rotate.Perform(transform, RotationSpeed);
        }
        if (IsMoving)
        {
            Move.Perform(transform, MovementSpeed);
        }
    }


}
