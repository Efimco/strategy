using UnityEngine;

public class MoveStrategy : IStrategy
{
    public void Perform(Transform transform, float speed)
    {
        transform.Translate(Vector3.up * speed * Time.deltaTime);
    }


}
