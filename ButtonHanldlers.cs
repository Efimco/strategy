using UnityEngine;

public class ButtonHanldlers : MonoBehaviour
{
    public Player player;
    public void MoveToggle()
    {
        player.IsMoving = !player.IsMoving;
    }
    public void RotateToggle()
    {

        player.IsRotating = !player.IsRotating;
    }
    public void EmmitToggle()
    {
        player.IsEmmit = !player.IsEmmit;
    }

}
