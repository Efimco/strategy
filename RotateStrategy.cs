using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateStrategy : IStrategy
{
    public void Perform(Transform transform, float speed)
    {
        transform.Rotate(new Vector3(0, 0, 1) * speed * Time.deltaTime);
    }
}
