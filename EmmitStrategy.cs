using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmmitStrategy : IStrategy
{
    public void Perform(Transform transform, float rate)
    {
        ParticleSystem particles = transform.GetComponent<ParticleSystem>();
        particles.emissionRate = rate;
        

    }
}
